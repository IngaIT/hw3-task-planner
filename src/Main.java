import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String [][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "prepare presentation";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "meet with clients";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "work on project";
        schedule[5][0] = "Friday";
        schedule[5][1] = "attend team meeting";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "relax and watch film";

        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("Please, input the day of the week: ");
            String input = scanner.nextLine().trim().toLowerCase();

            if (input.equals("exit")){
                break;
            }

            String task = null;

            switch(input){
                case "monday":
                    task = schedule[1][1];
                    break;
                case "tuesday":
                    task = schedule[2][1];
                    break;
                case "wednesday":
                    task = schedule[3][1];
                    break;
                case "thursday":
                    task = schedule[4][1];
                    break;
                case "friday":
                    task = schedule[5][1];
                    break;
                case "saturday":
                    task = schedule[6][1];
                    break;
                case "sunday":
                    task = schedule[0][1];
                    break;
            }
            if (task != null){
                System.out.println("Your tasks for " + input + ": " + task);
            }else{
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
        System.out.println("Program finished.");
    }
}